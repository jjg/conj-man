abbandonare	; to abandon
abitare		; to live, inhabit
accadere	; to happen, occur
accendere	; to switch on
accettare	; to accept
accogliere	; to welcome
accompagnare	; to accompany
accorger	; to recognise
affermare	; to state, say, affirm
affrontare	; to deal with
aggiungere	; to add, append
aiutare		; to help
allontanare	; to turn away
alzare		; to lift, stand, get up
amare		; to love
ammazzare	; to kill
ammettere	; to admit
andare		; to go
annunciare	; to announce
apparire	; to appear
appartenere	; to belong
appoggiare	; to support
aprire		; to trip
armare		; to arm, fit out
arrestare	; to stop, arrest
arrivare	; to arrive
ascoltare	; to hear, listen
aspettare	; to wait, expect
assicurare	; to assure, secure, guarantee
assistere	; to assist
assumere	; to assume, take on, hire
attaccare	; to attack, attach, stick
attendere	; to await
attraversare	; to traverse
aumentare	; to increase, raise, enhance
avanzare	; to advance
avere		; to have
avvenire	; to occur, come about
avvertire	; to alert
avvicinare	; to approach
baciare		; to kiss
badare		; to look after, be careful
bastare		; to suffice
battere		; to beat
bere		; to drink
bisognare	; to be necessary
bruciare	; to burn
buttare		; to throw
cadere		; to fall, drop
cambiare	; to change
camminare	; to walk
cantare		; to sing
capire		; to understand
capitare	; to happen, occur
celebrare	; to celebrate
cercare		; to search (for)
chiamare	; to call, phone, summon
chiedere	; to ask (for)
chiudere	; to close
colpire		; to hit
cominciare	; to commence
compiere	; to fulfill, accomplish
comporre	; to compose, dial
comprendere	; to understand, realise
comprare	; to buy
concedere	; to concede, grant
concludere	; to conclude, close
condurre	; to conduct
confessare	; to confess
conoscere	; to know
consentire	; to allow
conservare	; to preserve, conserve
considerare	; to consider
contare		; to count, enumerate
contenere	; to contain
continuare	; to continue
convincere	; to convince
coprire		; to cover, be covered
correre		; to run
costituire	; to constitute
costringere	; to force
costruire	; to build (up)
creare		; to create
credere		; to believe
crescere	; to grow (up)
dare		; to give
decidere	; to decide
dedicare	; to devote, dedicate
descrivere	; to describe
desiderare	; to want
determinare	; to determine
dichiarare	; to declare, state
difendere	; to defend
diffondere	; to spread, disseminate, popularise
dimenticare	; to forget
dimostrare	; to demonstrate
dipendere	; to depend
dire		; to say, mean
dirigere	; to direct
discutere	; to discuss, argue
disporre	; to dispose, order, coordinate
distinguere	; to distinguish
distruggere	; to destroy
diventare	; to become
divenire	; to become (formal) 
divertire	; to entertain, amuse (oneself)
dividere	; to divide
domandare	; to ask
dormire		; to sleep
dovere		; to have to
durare		; to last, persist
elevare		; to elevate
entrare		; to enter
escludere	; to exclude
esistere	; to exist
esporre		; to expose, set forth
esprimere	; to express
essere		; to be
estendere	; to extend
evitare		; to avoid
ferire		; to hurt, injure
fermare		; to stop
figurare	; to appear, make a good impression
finire		; to end (up), cease
fissare		; to fix, fasten
fondare		; to found, establish
formare		; to form, train
fornire		; to provide, furnish
fuggire		; to escape, flee
fumare		; to smoke
gettare		; to throw
giocare		; to play, gamble
girare		; to turn, spin
giudicare	; to judge
giungere	; to reach
godere		; to enjoy
gridare		; to shout (out)
guardare	; to watch, look (at)
guidare		; to drive, guide
immaginare	; to imagine
imparare	; to learn
impedire	; to prevent
importare	; to import, matter
imporre		; to impose
incontrare	; to encounter, meet
indicare	; to indicate
iniziare	; to begin
innamorare	; to fall in love
insegnare	; to teach, instruct
insistere	; to insist
intendere	; to mean
interessare	; to interest
invitare	; to invite
lanciare	; to launch, throw
lasciare	; to leave, allow
lavorare	; to work, operate, labour
legare		; to tie
leggere		; to read
levare		; to lift, get up, take out 
liberare	; to free
limitare	; to limit
mancare		; to miss, lack, fail
mandare		; to send (off, out)
mangiare	; to eat
mantenere	; to maintain
meritare	; to merit, deserve
mettere		; to put (on)
morire		; to die
mostrare	; to show, display
muovere		; to move
nascere		; to be born
nascondere	; to hide, be hidden
notare		; to note, notice
occorrere	; to take, be needed
offendere	; to offend
offrire		; to offer, provide
opporre		; to offer (resistance)
ordinare	; to sort, order (goods)
organizzare	; to organise
osservare	; to observe, watch
ottenere	; to get, obtain
pagare		; to pay (up)
parere		; to think
parlare		; to speak, talk
partecipare	; to participate
partire		; to leave, depart
passare		; to switch, pass, go through
pensare		; to think
perdere		; to lose, miss
permettere	; to permit, allow
pesare		; to weigh
piacere		; to like, please
piangere	; to cry, weep
piantare	; to plant
porre		; to put
portare		; to bring, carry
posare		; to lay, pose
possedere	; to possess, have
potere		; to be able to, likely to
preferire	; to prefer
pregare		; to pray, beg
prendere	; to take, catch, pick up
preoccupare	; to worry
preparare	; to perpare
presentare	; to submit, present
prevedere	; to provide, forecast
procedere	; to proceed
produrre	; to manufacture
promettere	; to promise
proporre	; to propose, offer
provare		; to try
provocare	; to cause, provoke
provvedere	; to cater, provide
pubblicare	; to publish
raccogliere	; to collect, gather
raccontare	; to tell, recount
raggiungere	; to reach
rappresentare	; to represent
recare		; to cause
rendere		; to make, render, yield interest
resistere	; to resist
restare		; to remain
ricevere	; to receive
richiedere	; to request, require
riconoscere	; to recognise
ricordare	; to remember
ridere		; to laugh
ridurre		; to reduce
riempire	; to fill (in), be filled
rientrare	; to re-enter, be part of 
riferire	; to report, refer
rifiutare	; to decline, refuse
riguardare	; to concern
rimanere	; to remain
rimettere	; to return
ringraziare	; to thank
ripetere	; to repeat
riportare	; to report, bring/take back
riprendere	; to resume
risolvere	; to solve, resolve
rispondere	; to respond
risultare	; to result
ritenere	; to believe
ritornare	; to return
ritrovare	; to find
riunire		; to gather
riuscire	; to succeed
rivedere	; to review, revise
rivelare	; to reveal
rivolgere	; to turn
rompere		; to break
salire		; to go up, rise, climb
saltare		; to jump
salutare	; to say goodbye, greet
salvare		; to save
sapere		; to know (how)
sbagliare	; to err
scappare	; to escape, flee
scegliere	; to choose
scendere	; to get off
scherzare	; to joke
scomparire	; to disappear
scoppiare	; to burst, break out
scoprire	; to discover
scorrere	; to slide, flow
scrivere	; to write
scusare		; to excuse, justify
sedere		; to sit
segnare		; to mark, score
seguire		; to follow (back)
sembrare	; to seem
sentire		; to feel, sense, hear
servire		; to serve
significare	; to mean, signify
smettere	; to stop
soffrire	; to suffer
sognare		; to dream
sorgere		; to rise
sorprendere	; to surprise
sorridere	; to smile
sostenere	; to support, hold up
spegnere	; to blow out, turn off
sperare		; to hope
spiegare	; to explain
spingere	; to push
sposare		; to marry
stabilire	; to establish
staccare	; to remove
stare		; to stay
stringere	; to tighten
studiare	; to study
succedere	; to happen
suonare		; to play
superare	; to overcome
svegliare	; to wake
svolgere	; to perform
tacere		; to hush, be silent
tagliare	; to cut (off)
temere		; to fear
tendere		; to stretch
tenere		; to hold
tentare		; to attempt
tirare		; to pull, shoot
toccare		; to touch
togliere	; to remove
tornare		; to return
trarre		; to draw, pull
trascinare	; to drag
trasformare	; to transform
trattare	; to deal (with)
trovare		; to find
uccidere	; to kill
udire		; to hear
unire		; to join, merge
usare		; to use
uscire		; to exit
valere		; to be worth
vedere		; to see, view
vendere		; to sell
venire		; to come
vestire		; to dress
vincere		; to win
vivere		; to live
volare		; to fly
volere		; to want
volgere		; to turn
voltare		; to turn
